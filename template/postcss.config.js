module.exports = {
  plugins: {
    autoprefixer: {},
    // dev模式下，自定义element-ui样式会出现样式多次重复，启用cssnano插件去除重复
    cssnano: {}
  }
}
