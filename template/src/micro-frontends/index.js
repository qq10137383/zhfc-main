import { registerMicroApps, start, addGlobalUncaughtErrorHandler } from 'qiankun-dhcc'
import { readEnvBoolean } from './util'
import emitter from './emitter'
import errorHandler from './errorHandler'
import microApp, { initMicroApp } from './microApp'
import tagsView, { initTagsView } from './tagsView'
import user, { initUser } from './user'
import * as auth from '@/utils/auth'

// 微前端启动配置
let options = {
    // 预加载
    prefetch: readEnvBoolean('VUE_APP_MICRO_PREFETCH'),
    // 样式隔离(增加微应用前缀)
    sandbox: {
        experimentalStyleIsolation: readEnvBoolean('VUE_APP_MICRO_STYLE_ISOLATION')
    },
    // keepAlive缓存
    keepAlive: readEnvBoolean('VUE_APP_MICRO_KEEP_ALIVE')
}

/**
 * 初始化微前端
 * @param {Vue} inst vue根实例 
 */
export function initMicroFrontends(inst) {
    initMicroApp(inst)
    initTagsView(inst)
    initUser(inst)
}

/**
 * 注册微前端配置
 */
export function registerMicroFrontends(apps) {
    const config = apps.map(app => {
        return {
            // 微应用注册配置
            ...app.registerConfig,
            // 主应用下发到子应用接口
            props: {
                emitter,
                microApp,
                tagsView,
                user,
                auth,
                options
            }
        }
    })
    registerMicroApps(config)
}

// qiankun启动标志
let started

/**
 * 启动微前端
 */
export function startMicroFrontends() {
    if (!started) {
        start(options)
        started = true
    }
}

// 微前端全局异常处理
addGlobalUncaughtErrorHandler(errorHandler)