import emitter from './emitter'
import { parseExtOpt, deepCopy } from './util'

// 微应用切换事件
const EVENT_APP_CHANGE = 'microApp-app-changed'
// 侧边栏改变事件
const EVENT_SIDEBAR_CHANGE = 'microApp-sidebar-changed'
// element组件size改变
const EVENT_ELEMENT_SIZE_CHANGE = 'microApp-element-size-changed'
// 应用设置改变事件
const EVENT_SETTINGS_CHANGE = 'microApp-settings-changed'

// 默认微应用起始端口
let devPort = 10000
// vue实例
let vueInst

/**
 * 初始化微应用
 * @param {Vue} inst vue实例
 */
export function initMicroApp(inst) {
    vueInst = inst

    inst.$watch(() => {
        return inst.$store.state.microApp.app
    }, (val) => {
        emitter.emit(EVENT_APP_CHANGE, val)
    })

    inst.$watch(() => {
        return inst.$store.state.app.sidebar.opened
    }, (val) => {
        emitter.emit(EVENT_SIDEBAR_CHANGE, val)
    })
    inst.$watch(() => {
        return inst.$store.state.app.size
    }, (val) => {
        emitter.emit(EVENT_ELEMENT_SIZE_CHANGE, val)
    })

    inst.$watch(() => {
        return inst.$store.state.settings
    }, (val) => {
        emitter.emit(EVENT_SETTINGS_CHANGE, val)
    }, { deep: true })
}

/**
 * 获取微应用地址(开发环境使用devPort，构建环境使用prodUrl)
 * @param {Object} app 
 * @returns 
 */
function _getAppEntry(meta) {
    let entry
    if (process.env.NODE_ENV === 'development') {
        const port = meta.devPort || ++devPort
        entry = `http://localhost:${port}`
    }
    else {
        entry = meta.prodUrl
    }
    return entry
}

/**
 * 创建微应用注册信息
 * @param {Array} apps 微应用信息
 * @returns 
 */
export function createMicroAppInfos(apps) {
    return apps.map((app) => {
        const { code, extOpt } = app
        const meta = parseExtOpt(extOpt)
        app.icon = app.icon || meta.icon || 'guide'
        return {
            ...app,
            // 微应用注册配置
            registerConfig: {
                name: code, // 微应用名
                entry: _getAppEntry(meta), // 应用地址
                container: `#${code}-wrap`, // 挂载dom容器
                activeRule: `/${code}` // 激活规则
            }
        }
    })
}

/**
 * 获取所有应用信息(包括主应用)
 * @returns 
 */
function getAllAppInfos() {
    const { appInfos } = vueInst.$store.state.microApp
    return deepCopy(appInfos)
}

/**
 * 获取所有微应用信息(不包括主应用)
 */
function getMicroAppInfos() {
    const apps = getAllAppInfos()
    return apps.filter(m => !m.__isMain)
}

/**
 * 获取应用信息
 * @param {String} appName 应用名
 */
function getAppInfo(appName) {
    const { appInfos } = vueInst.$store.state.microApp
    const app = appInfos.find(app => app.code === appName)
    return deepCopy(app)
}

/**
 * 获取当前激活的应用信息
 */
function getCurrentAppInfo() {
    const { app } = vueInst.$store.state.microApp
    return getAppInfo(app)
}

/**
 * 拷贝菜单
 * @param {Array} srcMenus 源菜单
 * @param {Array} tarMenus 目标菜单
 */
export function _cloneMenus(srcMenus, tarMenus) {
    srcMenus.forEach(menu => {
        // 去掉组件信息(component)
        // eslint-disable-next-line no-unused-vars
        const { component, children, ...rest } = menu
        const item = deepCopy(rest)
        if (children) {
            item.children = []
            _cloneMenus(children, item.children)
        }
        tarMenus.push(item)
    })
}

/**
 * 获取所有应用菜单(包括主应用)
 */
function getAllAppMenus() {
    const menus = []
    const { appRoutes } = vueInst.$store.state.microApp
    _cloneMenus(appRoutes, menus)
    return menus
}

/**
 * 获取所有微应用菜单(不包括主应用)
 */
function getMicroAppMenus() {
    const menus = getAllAppMenus()
    menus.filter(menu => menu.meta.appName != process.env.VUE_APP_NAME)
    return menus
}

/**
 * 获取菜单信息
 * @param {String} appName 应用名
 */
function getAppMenu(appName) {
    const menus = getAllAppMenus()
    menus.filter(menu => menu.meta.appName == appName)
    return menus
}

/**
 * 获取当前激活的应用菜单
 */
function getCurrentMenus() {
    const { app } = vueInst.$store.state.microApp
    return getAppMenu(app)
}

/**
 * 获取侧边栏打开状态
 * @returns 
 */
function getSidebarStatus() {
    return vueInst.$store.state.app.sidebar.opened
}

/**
 * 获取element-ui组件size
 */
function getElementSize() {
    return vueInst.$store.state.app.size
}

/**
 * 获取应用设置信息
 * @returns 
 */
function getAppSettings() {
    return deepCopy(vueInst.$store.state.settings)
}

/**
 * 切换应用菜单
 * @param {String} appName 
 */
function setApp(appName) {
    vueInst.$store.commit("microApp/SET_APP", appName);
    vueInst.$store.commit("app/SET_SIDEBAR", true);
}

/**
 * 设置侧边栏打开状态
 * @params {Boolean} opened
 */
function setSidebarStatus(opened) {
    vueInst.$store.commit("app/SET_SIDEBAR", opened);
}

/**
 * 设置element-ui组件size
 * @param {String} size 
 */
function setElementSize(size) {
    vueInst.$store.commit("app/SET_SIZE", size);
}

/**
 * 修改应用设置
 * @param {Object} settings 
 */
function setAppSettings(settings) {
    vueInst.$store.commit("settings/CHANGE_SETTING", settings);
}

export default {
    EVENT_APP_CHANGE,
    EVENT_SIDEBAR_CHANGE,
    EVENT_ELEMENT_SIZE_CHANGE,
    EVENT_SETTINGS_CHANGE,
    MAIN_APP_NAME: process.env.VUE_APP_NAME,
    getAllAppInfos,
    getMicroAppInfos,
    getAppInfo,
    getCurrentAppInfo,
    getAllAppMenus,
    getMicroAppMenus,
    getAppMenu,
    getCurrentMenus,
    getSidebarStatus,
    getElementSize,
    getAppSettings,
    setApp,
    setSidebarStatus,
    setElementSize,
    setAppSettings
}