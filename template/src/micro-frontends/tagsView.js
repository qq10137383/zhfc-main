import emitter from './emitter'
import { deepCopy } from './util'

// 打开页签事件
const EVENT_OPEN_VIEW = 'tabview-open'
// 刷新页签事件
const EVENT_REFRESH_VIEW = 'tabview-refresh'
// 关闭页签事件
const EVENT_CLOSE_VIEW = 'tabview-close'
// 关闭其他页签事件
const EVENT_CLOSE_OTHERS_VIEW = 'tabview-close-others'
// 关闭所有页签事件
const EVENT_CLOSE_ALL_VIEW = 'tabview-close-all'
// 页签改变事件
const EVENT_VISITED_CHANGE = 'tabview-visited-changed'
// 缓存页签改变事件
const EVENT_CACHED_CHANGE = 'tabview-cached-changed'

// vue实例
let vueInst
// 页签store
let tagsView

/**
 * 拷贝页签(深拷贝会导致循环依赖，需要手动深拷贝)
 * @param {Object} view 
 * @returns 
 */
export function cloneView(view) {
    return {
        name: view.name,
        title: view.title,
        path: view.path,
        fullPath: view.fullPath,
        hash: view.hash,
        params: view.params ? deepCopy(view.params) : {},
        query: view.query ? deepCopy(view.query) : {},
        meta: view.meta ? deepCopy(view.meta) : {},
    }
}

/**
 * 初始化页签
 * @param {Vue} inst vue实例
 */
export function initTagsView(inst) {
    vueInst = inst
    tagsView = inst.$store.state.tagsView

    inst.$watch(() => {
        return tagsView.visitedViews
    }, (val) => {
        const views = val.map(m => cloneView(m))
        emitter.emit(EVENT_VISITED_CHANGE, views)
    })

    inst.$watch(() => {
        return tagsView.cachedViews
    }, (val) => {
        emitter.emit(EVENT_CACHED_CHANGE, deepCopy(val))
    })
}

/**
 * 事件调用(内部使用)
 * @param {String} eventName 事件名 
 * @param {any} args 事件参数
 */
export function invokeTagsViewEvent(eventName, args) {
    let result
    if (Array.isArray(args)) {
        result = args.map(m => cloneView(m))
    }
    else {
        result = cloneView(args)
    }
    emitter.emit(eventName, result)
}

/**
 * 获取所有页签
 * @returns 
 */
function getVisitedViews() {
    return tagsView.visitedViews.map(m => cloneView(m))
}

/**
 * 页签是否属于某应用
 * @param {String} appName 
 * @param {string} view 
 * @returns 
 */
function isAppView(appName, view) {
    return view.meta.appName === appName
}

/**
 * 获取某微应用所有页签
 * @param {string} appName 微应用名
 */
function getAppVisitedViews(appName) {
    return getVisitedViews().filter(m => isAppView(appName, m))
}

/**
 * 获取所有缓存页签
 * @returns 
 */
function getCachedViews() {
    return deepCopy(tagsView.cachedViews)
}

/**
 * 获取某微应用所有缓存页签
 * @param {string} appName 微应用名
 */
function getAppCachedViews(appName) {
    let views = getCachedViews()
    views = views.reduce((result, m) => {
        const segs = m.split('#')
        if (segs.length === 2 && segs[0] === appName) {
            result.push(segs[1])
        }
        return result
    }, [])
    return views;
}

/**
 * 转换路由路径
 * @param {Object} options 
 * -- options.appName {String} 应用名称
 * -- options.path {String} 应用页签路径
 */
function _resolveViewPath(options) {
    const { appName, path } = options
    let value = path
    if (appName && appName !== process.env.VUE_APP_NAME) {
        value = `/${appName}${path}`
    }
    return value
}

/**
 * 跳转到最后一个页签
 */
function _toLastView() {
    const latestView = tagsView.visitedViews.slice(-1)[0];
    if (latestView) {
        vueInst.$router.push(latestView.fullPath);
    } else {
        vueInst.$router.push("/");
    }
}

/**
 * 打开页签
 * @param {Object} options
 * -- options.appName {String} 应用名称，主应用(基座)传main，微应用传应用名
 * -- options.path {String} 应用页签路径
 * -- options.params {Object} 路由路径参数
 * -- options.query {Object} 路由查询参数
 */
function openView(options) {
    return new Promise((resolve, reject) => {
        options.path = _resolveViewPath(options)
        vueInst.$router.push(options, (route) => {
            resolve(route)
        }, (error) => {
            reject(error)
        })
    })
}

/**
 * 关闭页签
 * @param {Object} options 
 * -- options.appName {String} 应用名称，主应用(基座)传main，微应用传应用名
 * -- options.path {String} 应用页签全路径(包括路径参数和查询参数)
 */
function closeView(options) {
    return new Promise((resolve, reject) => {
        let path = _resolveViewPath(options)
        const view = tagsView.visitedViews.find(m => m.path === path)
        if (!view) {
            reject(`closeView error，${path} is not found！`)
            return
        }
        vueInst.$store.dispatch('tagsView/delView', view).then(() => {
            _toLastView()
            resolve(cloneView(view))
        })
    })
}

/**
 * 关闭当前页签
 */
async function closeCurrentView() {
    return new Promise((resolve) => {
        vueInst.$store.dispatch('tagsView/delView', vueInst.$route)
            .then(() => {
                _toLastView()
                resolve(cloneView(vueInst.$route))
            });
    })
}

/**
 * 刷新页签
 * @param {Object} options 
 * -- options.appName {String} 微应用名称，主应用(基座)传main，微应用传应用名
 * -- options.path {String} 基座或微应用页签全路径(包括路径参数和查询参数)
 */
function refreshView(options) {
    return new Promise((resolve, reject) => {
        let path = _resolveViewPath(options)
        const view = tagsView.visitedViews.find(m => m.path === path)
        if (!view) {
            reject(`refreshView error，${path} is not found！`)
            return
        }
        vueInst.$store.dispatch('tagsView/refreshView', view).then(() => {
            resolve(cloneView(view))
        })
    })
}

/**
 * 刷新当前视图
 * @returns 
 */
function refreshCurrentView() {
    return new Promise((resolve) => {
        vueInst.$store.dispatch('tagsView/refreshView', vueInst.$route).then(() => {
            resolve(cloneView(vueInst.$route))
        })
    })
}

export default {
    EVENT_OPEN_VIEW,
    EVENT_REFRESH_VIEW,
    EVENT_CLOSE_VIEW,
    EVENT_CLOSE_OTHERS_VIEW,
    EVENT_CLOSE_ALL_VIEW,
    EVENT_VISITED_CHANGE,
    EVENT_CACHED_CHANGE,
    isAppView,
    getVisitedViews,
    getAppVisitedViews,
    getCachedViews,
    getAppCachedViews,
    openView,
    closeView,
    closeCurrentView,
    refreshView,
    refreshCurrentView
}