import { Layout, Blank } from '@/layout'
import { parseExtOpt } from './util'

/**
 * 处理主应用路由
 */
export function processMainAppRoutes(routes) {
  routes.forEach(route => {
    _parseMainAppRoute(route)
    if (route.children) {
      route.children.forEach(child => _parseMainAppRoute(child))
    }
  })
  return routes
}

/**
 * 转换主应用路由信息(增加应用名)
 */
function _parseMainAppRoute(route) {
  const meta = route.meta || (route.meta = {})
  meta.appName = process.env.VUE_APP_NAME
}

/**
 * 从路由路径中解析路由名
 * @param {String} path 路由路径
 * @returns 
 */
function _getRouteNameFromPath(path) {
  let name = ''
  // 取路径最后一截
  const segs = path.match(/.*\/(.*)$/)
  if (segs && segs.length === 2) {
    name = segs[1]
  }
  // 首字符转大写
  if (name && name.length > 1) {
    name = name.substring(0, 1).toUpperCase() + name.substring(1)
  }
  return name
}

/**
 * 应用菜单转路由
 * @param {Object} app 应用信息
 * @param {Object} menu 菜单信息 
 * @returns 
 */
function _parseMicroAppMenuRoute(app, menu) {
  const route = {}
  // 解析路由元数据
  const meta = route.meta = parseExtOpt(menu.extOpt)
  meta.menu = menu // menu
  meta.appName = app.code    // 路由所属系统
  meta.title = meta.title || menu.name  // title
  meta.icon = meta.icon || menu.icon || (menu.pid ? 'component' : 'list')  // icon

  // 路由路径增加微应用前缀(功能组没有path，但是vue-router path是必须的，需要用id生成一个唯一无用路径)
  const path = meta.path || menu.pageAddr // path
  route.path = `/${app.code}${path || `/${menu.id}`}`

  // 对于功能项name是必须的(页签、缓存使用name)，没有name则默认从路径中生成name
  const name = meta.name || _getRouteNameFromPath(path)
  // 路由名称增加微应用名(防止重复)
  if (name) {
    route.name = `${app.code}#${name}`
  }

  // 路由组件，子路由使用Blank，基路由使用Layout
  route.component = menu.pid ? Blank : Layout

  // 路由是否隐藏(默认值:false)
  if ('hidden' in meta) {
    route.hidden = !!meta.hidden
  }
  // 菜单组只有一个子菜单时总是显示菜单组(默认值:菜单组：true，菜单项：false)
  if ('alwaysShow' in meta) {
    route.alwaysShow = !!meta.alwaysShow
  }
  else {
    route.alwaysShow = !menu.pid
  }
  // 路由重定向路径增加微应用前缀
  if (meta.redirect) {
    route.redirect = `/${app.code}${meta.redirect}`
  }
  // 路由激活菜单增加微应用前缀
  if (meta.activeMenu) {
    meta.activeMenu = `/${app.code}${meta.activeMenu}`
  }
  return route
}

/**
 * 生成子系统根路径
 * @param {Object} app 
 * @returns 
 */
export function _parseMicroAppRoute(app) {
  const route = {}
  route.name = `${app.code}#Portal`; // 组件名称默认为Portal
  route.path = `/${app.code}` // 路径为子系统根路由
  route.hidden = true;  // 不在菜单中显示，仅为了避免访问子系统根路由出现404
  route.component = Layout

  const meta = {}
  meta.appName = app.code    // 路由所属系统
  meta.title = app.name  // title
  meta.icon = 'list' // icon
  route.meta = meta
  return route
}

/**
 * 生成微应用配置路由
 * @param {Array} apps 应用信息 
 * @param {Array} menus 菜单信息 
 * @returns 
 */
export function createMicroAppRoutes(apps, menus) {
  const appMap = apps.reduce((map, app) => {
    map[app.id] = app
    return map
  }, {})

  // 菜单信息转路由
  const menuMap = menus.reduce((map, menu) => {
    const app = appMap[menu.appId]
    const route = _parseMicroAppMenuRoute(app, menu)
    map[menu.id] = route
    return map
  }, {})
  // 生成路由上下级关系
  const routes = Object.values(menuMap).reduce((result, item) => {
    const { menu } = item.meta
    if (!menu.pid) {
      result.push(item)
      return result
    }
    const parent = menuMap[menu.pid]
    if (parent) {
      const children = parent.children || (parent.children = [])
      children.push(item)
    }
    return result
  }, [])

  // 生成子系统根路径
  apps.forEach((app) => {
    const route = _parseMicroAppRoute(app);
    routes.push(route);
  });

  // 增加404路由
  routes.push({
    path: '*',
    redirect: '/404',
    hidden: true,
    meta: {
      appName: process.env.VUE_APP_NAME
    }
  })

  // 功能组生成重定向路由(重定向到功能组下第一个功能)
  routes.forEach(item => {
    if (!item.redirect && item.children && item.children.length > 0) {
      item.redirect = item.children[0].path
    }
  })

  return routes
}