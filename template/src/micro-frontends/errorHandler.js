import { Message } from 'element-ui'
import microApp from './microApp'

// 异常处理器
const handlers = {
    // 加载微应用错误
    LOADING_SOURCE_CODE(app, error) {
        let message = `加载微应用【${app.name}】失败，`
        if (error.message.includes('Failed to fetch')) {
            message += `地址：${app.registerConfig.entry} 不可访问！`
        }
        else {
            message += `执行微应用代码出错！`
        }
        Message.error(message)
        console.error(message)
    },
    // 启动微应用
    BOOTSTRAPPING(app, error) {
        Message.error(`启动微应用【${app.name}】失败！`)
        console.error(error)
    },
    // 挂载微应用
    MOUNTING(app, error) {
        Message.error(`挂载微应用【${app.name}】失败！`)
        console.error(error)
    },
    // 默认处理
    DEFAULT(app, error) {
        const message = `微应用【${app.name}】发生错误，${error.message}`
        Message.error(message)
        console.error(message)
    }
}

// 异常代码
const codes = Object.keys(handlers).filter(c => c != 'DEFAULT')

/**
 * 微前端全局异常处理
 */
const errorHandler = ({ error }) => {
    if (!error) return

    const { appOrParcelName, message } = error
    if (!appOrParcelName) return

    // 对应错误代码处理
    let app = microApp.getAppInfo(appOrParcelName)
    for (const code of codes) {
        if (message.includes(code)) {
            handlers[code](app, error)
            return
        }
    }
    // 默认处理
    handlers.DEFAULT(app, error)
}

export default errorHandler