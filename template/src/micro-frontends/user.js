import emitter from './emitter'
import { deepCopy } from './util'

// 用户退出事件
const EVENT_USER_LOGOUT = 'user-logout'
// 用户信息改变事件
const EVENT_USER_INFO_CHANGE = 'user-info-change'

// vue实例
let vueInst
// 用户store
let user

/**
 * 初始化用户模块
 * @param {Vue} inst vue实例
 */
export function initUser(inst) {
    vueInst = inst
    user = inst.$store.state.user

    inst.$watch(() => {
        return user.token
    }, (val) => {
        if (!val) {
            emitter.emit(EVENT_USER_LOGOUT)
        }
    })

    let prevInfo = ''
    inst.$watch(() => {
        return user.userInfo
    }, (val) => {
        // 切换路由会重新获取用户信息，监听不准确，再通过对比字符串进一步判断
        const info = JSON.stringify(val)
        if (info != prevInfo) {
            emitter.emit(EVENT_USER_INFO_CHANGE, JSON.parse(info))
        }
        prevInfo = info
    })
}

/**
 * 用户退出
 */
export async function logout() {
    await vueInst.$store.dispatch('user/logout')
    // keep alive模式下微应用没有卸载方法，重新刷新
    location.reload()
    //vueInst.$router.push('/login')
}

/**
 * 获取当前登录用户信息
 */
export function getUserInfo() {
    return deepCopy(user.userInfo)
}

/**
 * 重置用户信息
 * @param {Boolean} toLogin 是否重置后退到登录页面
 */
export function resetUserInfo(toLogin) {
    vueInst.$store.dispatch('user/reset')
    if (toLogin) {
        location.reload()
        //vueInst.$router.push('/login')
    }
}

export default {
    EVENT_USER_LOGOUT,
    EVENT_USER_INFO_CHANGE,
    logout,
    getUserInfo,
    resetUserInfo
}