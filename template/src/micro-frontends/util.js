/**
 * 简单版本对象深拷贝
 * @param {Object} source 
 * @returns 
 */
export function deepCopy(source) {
    return JSON.parse(JSON.stringify(source))
}

/**
 * 转换配置扩展属性
 * @param {String} extOpt 
 * @returns 
 */
export function parseExtOpt(extOpt) {
    let options
    try {
        options = JSON.parse(extOpt || '{}')
    }
    catch {
        options = {}
    }
    return options
}

/**
 * 读环境变量Boolean值
 * @param {String} key 
 * @returns 
 */
export function readEnvBoolean(key) {
    const value = process.env[key]
    return /^true$/.test(value)
}