import request from '@/utils/request'

/**
 * 获取验证码
 * @returns 
 */
export function getKaptcha() { 
  return request({
    url: '/frame/index/getKaptcha.do',
    method: 'get'
  })
}

/**
 * 登录
 * @param {Object} data 
 * @returns 
 */
 export function login(data) {
  return request({
    url: '/frame/index/login.do',
    method: 'post',
    data
  })
}

/**
 * 获取用户信息
 * @returns 
 */
export function getInfo() { 
  return request({
    url: '/frame/index/getUser.do',
    method: 'get'
  })
}

/**
 * 退出登录
 * @returns 
 */
export function logout() { 
  return request({
    url: '/frame/index/logout.do',
    method: 'get'
  })
}

/**
 * 修改密码
 * @param {Object} data 
 * @returns 
 */
export function editPassword(data) { 
  return request({
    url: '/frame/index/changeMyPassword.do',
    method: 'post',
    data
  })
}

/**
 * 获得当前用户子系统
 * @returns 
 */
 export function listApps() { 
  return request({
    url: 'frame/index/getMyApp.do',
    method: 'get'
  })
}

/**
 * 获得当前用户菜单
 * @returns 
 */
 export function getMenu() {
  return request({
    url: '/frame/index/getMenu.do',
    method: 'get',
  })
}

/**
 * 获得当前用户对应子系统菜单
 * @param {Object} query 
 * @returns 
 */
export function getAppMenu(query) {
  return request({
    url: 'frame/index/getAppMenu.do',
    method: 'get',
    params: query
  })
}
