import { Table } from 'element-ui'

/**
 * 修复el-table组件在有些时候固定列错位的问题
 */
Table.methods.doLayout = function () {
    this.layout.updateColumnsWidth()
    if (this.shouldUpdateHeight) {
        this.layout.updateElsHeight()
    }
}