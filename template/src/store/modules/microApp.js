import { listApps, getMenu } from '@/api/user'
import settings from "@/settings"
import { constantRoutes } from "@/router"
import { createMicroAppInfos } from '@/micro-frontends/microApp'
import { createMicroAppRoutes } from '@/micro-frontends/router'

const state = {
  // 应用初始化标志
  inited: false,
  // 应用信息列表
  appInfos: [],
  // 应用路由
  appRoutes: [],
  // 当前激活的应用
  app: '',
}

const mutations = {
  SET_INITED: (state, inited) => {
    state.inited = inited
  },
  SET_APP_INFOS: (state, apps) => {
    // 合并主应用信息
    const { name, icon } = settings
    const main = {
      name,
      code: process.env.VUE_APP_NAME,
      icon,
      __isMain: true
    }
    state.appInfos = [main].concat(apps)
  },
  SET_APP_ROUTES: (state, routes) => {
    // 合并主应用路由
    state.appRoutes = constantRoutes.concat(routes)
  },
  SET_APP: (state, app) => {
    state.app = app
  }
}

const actions = {
  // 初始化应用
  async init({ commit }) {
    return new Promise((resolve, reject) => {
      return Promise.all([
        listApps(),
        getMenu()
      ]).then(([apps, menus]) => {
        // 生成微应用注册配置
        const appInfos = createMicroAppInfos(apps)
        // 生成微应用路由配置
        const routes = createMicroAppRoutes(apps, menus)

        commit('SET_APP_INFOS', appInfos)
        commit('SET_APP_ROUTES', routes)
        commit('SET_INITED', true)
        resolve([appInfos, routes])
      }).catch(error => {
        reject(error)
      })
    })
  },
  // 重置应用
  reset({ commit }) {
    commit('SET_APP_INFOS', [])
    commit('SET_APP_ROUTES', [])
    commit('SET_APP', '')
    commit('SET_INITED', false)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
