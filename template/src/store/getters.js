const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  appInited: state => state.microApp.inited,
  appInfos: state => state.microApp.appInfos,
  app: state => state.microApp.app,
  appRoutes: state => state.microApp.appRoutes,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  userInfo: state => state.user.userInfo,
  errorLogs: state => state.errorLog.logs
}
export default getters
