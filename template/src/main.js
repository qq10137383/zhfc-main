import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters
import * as components from './components' // global components
import * as directives from './directives' // global directives

import { initMicroFrontends } from '@/micro-frontends'

import './patch' // patch

Vue.use(Element, {
  size: Cookies.get('size') || process.env.VUE_APP_ELEMENT_SIZE, // set element-ui default size
})

// register global components
Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})
// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// register global directives
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})

Vue.config.productionTip = false

const instance = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

// 初始化微前端
initMicroFrontends(instance)