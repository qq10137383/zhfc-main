import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import { registerMicroFrontends } from '@/micro-frontends'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect', '/register'] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  /* has no token*/
  if (!hasToken) {
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
    return
  }

  // if is logged in, redirect to the home page
  if (to.path === '/login') {
    next({ path: '/' })
    NProgress.done() // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    return
  }

  // 路由守卫中通过user/getInfo检查用户生存期导致切换路由卡顿(请求0.5s才返回)，暂时只在request中判断
  // await store.dispatch('user/getInfo')

  try {
    // 初始化微应用
    const inited = store.getters.appInited
    if (!inited) {
      const bundles = await Promise.all([
        store.dispatch('user/getInfo'),
        store.dispatch('microApp/init')
      ])
      const [appInfos, routes] = bundles[1]
      // 增加动态路由
      router.addRoutes(routes)
      // 注册微前端
      registerMicroFrontends(appInfos)
      // hack method to ensure that addRoutes is complete
      // set the replace: true, so the navigation will not leave a history record
      next({ ...to, replace: true })
    }
    else {
      next()
    }
  }
  catch (error) {
    // remove token and go to login page to re-login
    await store.dispatch('user/reset')
    Message.error({ message: error || 'Has Error' })
    //next(`/login?redirect=${to.path}`)
    NProgress.done()
    location.reload()
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
