import Cookies from 'js-cookie'

// 验证token 
const TokenKey = 'uiticket'
export function getToken() {
  return Cookies.get(TokenKey)
}
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}
export function removeToken() {
  return Cookies.remove(TokenKey)
}

// 会话id
const SidKey = 'sid'
export function getSid() {
  return Cookies.get(SidKey)
}
export function setSid(sid) {
  return Cookies.set(SidKey, sid)
}
export function removeSid() {
  return Cookies.remove(SidKey)
}

// 验证码
const KaptKey = 'content-kapt'
export function getKapt() {
  return sessionStorage.getItem(KaptKey)
}
export function setKapt(kapt) {
  return sessionStorage.setItem(KaptKey, kapt)
}
export function removeKapt() {
  return sessionStorage.removeItem(KaptKey)
}

// 用户登录次数
const LoginContentKey = 'content-login'
export function getLoginContent() {
  return sessionStorage.getItem(LoginContentKey)
}
export function setLoginContent(count) {
  return sessionStorage.setItem(LoginContentKey, count)
}
export function removeLoginContent() {
  return sessionStorage.removeItem(LoginContentKey)
}

// 用户登录错误后被锁定时间
const LoginLockedKey = 'content-locked'
export function getLoginLocked() {
  return sessionStorage.getItem(LoginLockedKey)
}
export function setLoginLocked(content) {
  return sessionStorage.setItem(LoginLockedKey, content)
}
export function removeLoginLocked() {
  return sessionStorage.removeItem(LoginLockedKey)
}

/**
 * 设置验证headers信息
 * @param {Object} headers 
 */
export function setAuthHeaders(headers) {
  headers[TokenKey] = getToken()
  headers[SidKey] = getSid() || ''
}

/**
 * 设置登录headers信息
 * @param {Object} headers 
 */
export function setLoginHeaders(headers) {
  const kapt = getKapt()
  kapt && (headers[KaptKey] = kapt)

  const content = getLoginContent()
  content && (headers[LoginContentKey] = content)

  const locked = getLoginLocked()
  locked && (headers[LoginLockedKey] = locked)
}

/**
 * 存储登录返回headers信息
 * @param {Object} headers 
 */
export function storeLoginHeaders(headers) {
  const sid = headers[SidKey]
  sid && setSid(sid)

  const kapt = headers[KaptKey]
  kapt && setKapt(kapt)

  const content = headers[LoginContentKey]
  content && setLoginContent(content)

  const locked = headers[LoginLockedKey]
  locked && setLoginLocked(locked)
}

/**
 * 移除所有验证信息
 */
export function removeAll() {
  removeSid()
  removeToken()
  removeKapt()
  removeLoginContent()
  removeLoginLocked()
}