const path = require('path')
const fs = require('fs')
const spawn = require('child_process').spawn

/**
 * 依赖排序
 * @param {Object} data 
 */
exports.sortDependencies = function (data) {
    const packageJsonFile = path.join(process.cwd(), data.destDirName, "package.json")
    const packageJson = JSON.parse(fs.readFileSync(packageJsonFile))
    packageJson.devDependencies = sortObject(packageJson.devDependencies)
    packageJson.dependencies = sortObject(packageJson.dependencies)
    fs.writeFileSync(packageJsonFile, JSON.stringify(packageJson, null, 2) + '\n')
}

/**
 * 安装依赖
 * @param {string} cwd 
 * @param {string} executable 
 * @param {Function} color 
 */
exports.installDependencies = function (cwd, executable = 'npm', color) {
    console.log(`\n\n# ${color('安装项目依赖...')}`)
    console.log('# ========================\n')
    return runCommand(executable, ['install'], { cwd })
}

/**
 * 
 * @param {Object} data 
 * @param {Object} chalk 
 */
exports.printMessage = function (data, { green, yellow }) {
    const message = `
# ${green('项目初始化完成')}
# ========================

运行项目：

${yellow(`cd ${data.destDirName}\n${installMsg(data)}npm run dev`)}
`

    console.log(message)
}

/**
 * 安装提示信息
 * @param {Object} data 
 */
function installMsg(data) {
    return !data.autoInstall ? 'npm install (or yarn install)\n' : ''
}

/**
 * 开启子进程运行命令
 * @param {string} cmd 
 * @param {Array} args 
 * @param {Object} options 
 */
function runCommand(cmd, args, options) {
    return new Promise((resolve) => {
        const spwan = spawn(
            cmd,
            args,
            Object.assign(
                {
                    cwd: process.cwd(),
                    stdio: 'inherit',
                    shell: true,
                },
                options
            )
        )

        spwan.on('exit', () => {
            resolve()
        })
    })
}

/**
 * 对象按key排序
 * @param {Object} object 
 * @returns 
 */
function sortObject(object) {
    const sortedObject = {}
    Object.keys(object)
        .sort()
        .forEach(item => {
            sortedObject[item] = object[item]
        })
    return sortedObject
}