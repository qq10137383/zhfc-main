const path = require('path')
const pkg = require('./package.json')
const util = require('./utils')

module.exports = {
    helpers: {
        if_and(a, b, opts) {
            return a && b ? opts.fn(this) : opts.inverse(this)
        },
        template_version() {
            return pkg.version
        },
    },
    prompts: {
        name: {
            type: 'string',
            required: true,
            message: '请输入应用名',
        },
        port: {
            type: 'string',
            required: true,
            message: "请输入应用启动端口",
            validate(port) {
                let pass = /^[1-9]\d*$/.test(port) && 1 <= 1 * port && 1 * port <= 65535
                if (!pass) {
                    pass = '请输入有效的端口号'
                }
                return pass;
            }
        },
        title: {
            type: 'string',
            required: false,
            message: '请输入应用标题',
            default: '互联网+智慧住房”信息平台',
        },
        lint: {
            type: 'confirm',
            message: '是否使用eslint校验代码?',
        },
        unit: {
            type: 'confirm',
            message: '是否使用jest单元测试?',
        },
        autoInstall: {
            type: 'list',
            message:
                '项目创建完成后自动安装依赖?',
            choices: [
                {
                    name: '使用npm安装',
                    value: 'npm',
                },
                {
                    name: '使用yarn安装',
                    value: 'yarn',
                },
                {
                    name: '不安装',
                    value: false,
                },
            ],
        },
    },
    filters: {
        '.eslintrc.js': 'lint',
        '.eslintignore': 'lint',
        "tests/**/*": 'unit',
        "jest.config.js": 'unit'
    },
    complete(data, { chalk }) {
        // 依赖排序
        util.sortDependencies(data)
        // 依赖安装
        if (data.autoInstall) {
            const cwd = path.join(process.cwd(), data.destDirName)
            util.installDependencies(cwd, data.autoInstall, chalk.green)
                .then(() => util.printMessage(data, chalk))
        }
        else {
            util.printMessage(data, chalk)
        }
    }
}